import random

ingredience_svetla = {
    'Golf': ['maso', 'pikantní dresink', 'zelenina'],
    'Kuřecí stripsy': ['maso', 'zelenina'],
    'Sýrový mlsoun': ['sýr', 'pikantní dresink', 'vejce'],
    'Šunkový Eiffel': ['šunka', 'sýr', 'zelenina'],
    'Debrecínka': ['šunka', 'sýr'],
    'Cestovatel po Maďarsku': ['salám', 'zelenina', 'pikantní dresink'],
    'Pořez': ['maso', 'zelenina'],
    'Maxim': ['sýr', 'šunka', 'zelenina'],
    'Delicates': ['salám', 'šunka', 'vejce', 'zelenina'],
    'Route 66': ['maso', 'zelenina']
}

ingredience_tmava = {
    'Cestovatel po Francii': ['zelenina', 'maso'],
    'Trhané vepřové & čedar': ['maso', 'sýr', 'zelenina'],
    'Krůtí šunka': ['šunka', 'sýr']
}

svetle_sendvice = {
    'Tuňák a vejce': ['tuňák', 'vejce', 'zelenina'],
    'Šunkový s vajíčkem': ['šunka', 'vejce', 'zelenina'],
    'Mexické kuře': ['maso', 'zelenina', 'sýr'],
    'Šunka a sýr': ['maso', 'zelenina'],
    'Triple (šunka, kuře, vejce)': ['šunka', 'maso', 'vejce'],
    'Hovězí s pastrami': ['maso', 'zelenina']
}

tmave_sendvice = {
    'Kuře a slanina': ['maso', 'slanina', 'zelenina'],
    'Cestovatel po Holandsku': ['slanina', 'vejce'],
    'Sýr a vejce': ['sýr', 'vejce'],
    'Kuřecí proteinový sendvič': ['maso', 'zelenina'],
}

cochces = input('Chceš si vybrat na co máš chuť (1), zobrazit top 5 baget a sendvičů (2), nebo zkusit štěstí náhodným výběrem (3)? ').lower()

while cochces not in ['1','2','3']:
    print('Neplatný vstup! Zadejte čísla 1,2 nebo 3')
    cochces = input('Chceš si vybrat na co máš chuť (1), zobrazit top 5 baget a sendvičů (2), nebo zkusit štěstí náhodným výběrem (3)? ').lower()

if cochces == '1':
    typ_jidla = input('Chceš sendvič nebo bagetu? ').lower()

    while typ_jidla not in ['bageta', 'bagetu', 'sendvič']:
        print('Neplatný vstup! Zadej bagetu nebo sendvič.')
        typ_jidla = input('Chceš sendvič nebo bagetu? ').lower()

    if typ_jidla == 'sendvič':
        barva_sendvice = input('Chceš světlý nebo tmavý sendvič? ').lower()

        while barva_sendvice not in ['světlý', 'tmavý']:
            print('Neplatný vstup! Zadej světlý nebo tmavý.')
            barva_sendvice = input('Chceš světlý nebo tmavý sendvič? ').lower()

        zelenina_fuj = input('Chceš mít ve svém sendviči zeleninu? (Ano/ne) ').lower()

        while zelenina_fuj not in ['ano', 'ne']:
            print('Neplatný vstup! Zadej ano nebo ne.')
            zelenina_fuj = input('Chceš mít ve svém sendviči zeleninu? (Ano/ne) ').lower()

        vhodne_sendvice = []

        if barva_sendvice == 'světlý':
            for sendvic, ingredience in svetle_sendvice.items():
                obsahuje_zeleninu = 'zelenina' in ingredience
                if (zelenina_fuj == 'ano' and obsahuje_zeleninu) or (zelenina_fuj == 'ne' and not obsahuje_zeleninu):
                    vhodne_sendvice.append(sendvic)
        elif barva_sendvice == 'tmavý':
            for sendvic, ingredience in tmave_sendvice.items():
                obsahuje_zeleninu = 'zelenina' in ingredience
                if (zelenina_fuj == 'ano' and obsahuje_zeleninu) or (zelenina_fuj == 'ne' and not obsahuje_zeleninu):
                    vhodne_sendvice.append(sendvic)
        else:
            print('Chybný vstup! Zkus to ještě jednou.')
            exit()

        print('\n\033[1mVhodné sendviče pro tebe:\033[0m')
        if vhodne_sendvice:
            print(', '.join(vhodne_sendvice))
        else:
            print('Nemáme žádné vhodné sendviče podle tvých preferencí.')

    elif typ_jidla == 'bageta' or typ_jidla == 'bagetu':
    
        barva_bagety = input('Chceš tmavou nebo světlou bagetu? ').lower()
        while barva_bagety not in ['tmavá', 'světlá', 'tmavou', 'světlou', 'svetlou', 'tmavou']:
            print('Neplatný vstup! Zadej prosím "tmavou" nebo "světlou".')
            barva_bagety = input('Chceš tmavou nebo světlou bagetu? ').lower()

        zelenina_fuj = input('Chceš mít ve své bagetě zeleninu? (Ano/ne) ').lower()

        while zelenina_fuj not in ['ano', 'ne']:
            print('Neplatný vstup! Zadej ano nebo ne.')
            zelenina_fuj = input('Chceš mít ve své bagetě zeleninu? (Ano/ne) ').lower()
    
        vhodne_bile = []
        vhodne_tmave = []
    
        if barva_bagety.lower() in ['světlá', 'světlou','svetlou','svetla']:
        
            pikantni_dresink = input('Chceš mít pikantní dresink? (Ano/ne) ').lower()
            while pikantni_dresink not in ['ano', 'ne']:
                print('Neplatný vstup! Prosím zadej "ano" nebo "ne".')
                pikantni_dresink = input('Chceš mít pikantní dresink? (Ano/ne) ').lower()

            for bageta, ingredience in ingredience_svetla.items():
                obsahuje_zeleninu = 'zelenina' in ingredience
                obsahuje_pikantni_dresink = 'pikantní dresink' in ingredience
                if (zelenina_fuj == 'ano' and obsahuje_zeleninu) or (zelenina_fuj == 'ne' and not obsahuje_zeleninu):
                    if (pikantni_dresink == 'ano' and obsahuje_pikantni_dresink) or (pikantni_dresink == 'ne' and not obsahuje_pikantni_dresink):
                        vhodne_bile.append(bageta)
        elif barva_bagety.lower() in ['tmavá', 'tmavou','tmava']:
            for bageta, ingredience in ingredience_tmava.items():
                obsahuje_zeleninu = 'zelenina' in ingredience 
                if (zelenina_fuj == 'ano' and obsahuje_zeleninu) or (zelenina_fuj == 'ne' and not obsahuje_zeleninu):
                    vhodne_tmave.append(bageta)
        else:
            print('Chybný vstup! Zkus to ještě jednou.')
            exit()

        print('\n\033[1mVhodné bagety pro tebe:\033[0m')
        if vhodne_bile:
            print(', '.join(vhodne_bile))
        if vhodne_tmave:
            print(', '.join(vhodne_tmave))
    else: 
        print('Chybný vstup! Zkus to ještě jednou')
elif cochces == '2':
    bagety_rated = {
        'Golf': [float(8)],
        'Kuřecí stripsy': [float(8.7)],
        'Sýrový mlsoun': [float(8.15)],
        'Šunkový Eiffel': [float(7.2)],
        'Debrecínka': [float(7.6)],
        'Cestovatel po Maďarsku': [float(6.5)],
        'Pořez': [float(9.4)],
        'Maxim': [float(8.5)],
        'Delicates': [float(8.2)],
        'Route 66': [float(8.3)],
        'Cestovatel po Francii': [float(9.8)],
        'Trhané vepřové & čedar': [float(9)],
        'Krůtí šunka': [float(7.7)]
    }

    sorted_bagety = sorted(bagety_rated.items(), key=lambda x: x[1][0], reverse=True)

    top_5_bagety = sorted_bagety[:5]


    print("\n\033[1mTop 5 bagety s nejvyšším hodnocením:\033[0m")
    for bageta in top_5_bagety:
        print(f"{bageta[0]}: {bageta[1][0]}")

    sendvice_rated = {
        'Tuňák a vejce': [float(7.3)],
        'Šunkový s vajíčkem': [float(7.7)],
        'Mexické kuře': [float(8.4)],
        'Šunka a sýr': [float(7.5)],
        'Triple (šunka, kuře, vejce)': [float(7.9)],
        'Hovězí s pastrami': [float(7.4)],
        'Kuře a slanina': [float(9)],
        'Cestovatel po Holandsku': [float(9.4)],
        'Sýr a vejce': [float(7)],
        'Kuřecí proteinový sendvič': [float(9.8)],
    }

    sorted_sendvice = sorted(sendvice_rated.items(), key=lambda x: x[1][0], reverse=True)

    top_5_sendvice = sorted_sendvice[:5]

    print('\n\033[1mTop 5 sendvičů s nejvyšším hodnocením:\033[0m')
    for sendvic in top_5_sendvice:
        print(f'{sendvic[0]}: {sendvic[1][0]}')

else:
    nahodna_polozka_svetla = random.choice(list(ingredience_svetla.items()))
    nahodna_polozka_tmava = random.choice(list(ingredience_tmava.items()))
    nahodne_sendvice_svetle = random.choice(list(svetle_sendvice.items()))
    nahodne_sendvice_tmave = random.choice(list(tmave_sendvice.items()))

    finalni_nahodna_polozka = random.choice([nahodna_polozka_svetla, nahodna_polozka_tmava, nahodne_sendvice_svetle, nahodne_sendvice_tmave])

    print("\n\033[1mNáhodná vybraná bageta/sendvič:\033[0m")
    print(finalni_nahodna_polozka[0])